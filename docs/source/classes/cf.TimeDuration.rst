.. currentmodule:: cf
.. default-role:: obj


cf.TimeDuration
===============

.. currentmodule:: cf

.. autoclass:: TimeDuration
   :no-members:
   :no-inherited-members:

TimeDuration attributes
-----------------------

.. autosummary::
   :toctree: ../generated/
   :template: attribute.rst

   ~cf.TimeDuration.Units
   ~cf.TimeDuration.isint
   ~cf.TimeDuration.iso
     
   
TimeDuration methods
--------------------

.. autosummary::
   :nosignatures:
   :toctree: ../generated/
   :template: method.rst

   ~cf.TimeDuration.bounds
   ~cf.TimeDuration.copy
   ~cf.TimeDuration.equals
   ~cf.TimeDuration.equivalent
   ~cf.TimeDuration.inspect
   ~cf.TimeDuration.interval
   ~cf.TimeDuration.is_day_factor
   
