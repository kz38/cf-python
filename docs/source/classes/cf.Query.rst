cf.Query
========

.. currentmodule:: cf

.. autoclass:: Query

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Query.__init__
      ~Query.addattr
      ~Query.copy
      ~Query.dump
      ~Query.equals
      ~Query.equivalent
      ~Query.evaluate
      ~Query.inspect
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Query.attr
      ~Query.exact
      ~Query.operator
      ~Query.value
   
   